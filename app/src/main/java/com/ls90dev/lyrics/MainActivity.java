package com.ls90dev.lyrics;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private MainMenu mainMenu;
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.init();
    }

    private void initNavigation() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed (View view) {
                super.onDrawerClosed(view);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            public void onDrawerOpened (View view) {
                super.onDrawerOpened(view);
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    // TODO: When you need to publish app in values/string.xml set real ad for banner
    private void initAd() {

        AdView mAdView = (AdView) findViewById(R.id.adView1);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        interstitialAd();

    }

    private void interstitialAd() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstitial_ad_unit_id));

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
                mainMenu.loadMenu(R.layout.menu_search);
            }
        });

        requestNewInterstitial();
    }

    // TODO: When you need to publish app in values/string.xml set real ad for interstitial
    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        mInterstitialAd.loadAd(adRequest);
    }

    private void init() {

        DatabaseSystem dbms = new DatabaseSystem(this);
        Handler handler = new Handler();
        dbms.openOrCreateDb();
        dbms.closeDB();
        this.initAd();
        handler.postDelayed(new Runnable() {
            public void run() {
                initNavigation();
                mainMenu = new MainMenu(
                        MainActivity.this.getWindow().getDecorView().getRootView(),
                        MainActivity.this,
                        R.id.linearMenu);
            }
        }, 1000);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        setTitle(item.getTitle());
        if (id == R.id.nav_search) {
            // Handle the camera action
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            } else {
                this.mainMenu.loadMenu(R.layout.menu_search);
            }
        } else if (id == R.id.nav_my_list) {
            this.mainMenu.loadMenu(id);

        } else if (id == R.id.nav_add_list) {
            this.mainMenu.loadMenu(R.layout.menu_add_list);

        } else if (id == R.id.nav_add_song && tableSize()) {
            this.mainMenu.loadMenu(R.layout.menu_add_song);

        } else if (id == R.id.nav_report) {
            try {
                String[] to = {getResources().getString(R.string.support_email)};
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT,
                        getResources().getString(R.string.app_name) + " - " +
                                getResources().getString(R.string.c_and_s_subject));
                emailIntent.setType("message/rfc822");
                startActivity(Intent.createChooser(emailIntent,
                        getResources().getString(R.string.c_and_s_intent_title)));

            } catch (Exception e) {
                String message = getResources().getString(R.string.email_app);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_rate) {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=" + this.getPackageName())));
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + this.getPackageName())));
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean tableSize() {
        DatabaseSystem dbms = new DatabaseSystem(this);
        dbms.openOrCreateDb();
        int size = 0;
        size = dbms.tables.size();
        dbms.closeDB();
        if (size < 1) {
            Toast.makeText(this, getResources().getString(R.string.add_song_error), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
