package com.ls90dev.lyrics;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.widget.Toast;

public class SmartToast extends Toast {
    /**
     * Construct an empty Toast object.  You must call {@link #setView} before you
     * can call {@link #show}.
     *
     * @param context The context to use.  Usually your {@link Application}
     *                or {@link Activity} object.
     */

    private static boolean messageShowing = false;

    public SmartToast(Context context, Toast toast) {
        super(context);
        this.setView(toast.getView());
    }

    public void show() {
        int duration = 2000;
        if (!messageShowing) {
            messageShowing = true;
            super.show();
            android.os.Handler handler = new android.os.Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    messageShowing = false;
                }
            }, duration);
        }
    }
}
