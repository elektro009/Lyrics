package com.ls90dev.lyrics;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class MainMenu {
    private View view;
    private Context ctx;
    private LinearLayout linearView;
    private LayoutInflater inflater;

    private EditText etCommon;
    private Spinner spinner;
    public Button bCommon;

    private String selectedItem = null;

    private String[] typeSearch = new String[]{"name", "title", "text_pattern"};

    private EditText renameList = null;

    private int minATLength = 2;
    private int minTexLength = 10;

    public MainMenu(View view, Context ctx, int menuID) {
        this.view = view;
        this.ctx = ctx;
        this.linearView = (LinearLayout) view.findViewById(menuID);
        this.inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void loadMenu(int layout) {

        if(layout == R.id.nav_my_list) {
            this.myListInit();
        } else {
            this.linearView.removeAllViews();
            View rl = inflater.inflate(layout, null);
            this.linearView.addView(rl);
        }

        if (layout == R.layout.menu_search) {
            this.searchInit();

        } else if (layout == R.layout.menu_add_list) {
            this.addListInit();
        } else if (layout == R.layout.menu_add_song) {
            initAddSong();
        }
    }

    private void searchInit() {
        this.spinner = (Spinner) view.findViewById(R.id.spinner_search);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx,
                android.R.layout.simple_spinner_dropdown_item,
                ctx.getResources().getStringArray(R.array.search_type));
        this.spinner.setAdapter(adapter);

        this.etCommon = (EditText)view.findViewById(R.id.et_search);
        this.bCommon = (Button)view.findViewById(R.id.b_search);
        this.bCommon.setOnClickListener(this.searchOCL);
    }

    private View.OnClickListener searchOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String etSearchValue = MainMenu.this.etCommon.getText().toString();
            SmartToast smartToast = null;
            int pos = MainMenu.this.spinner.getSelectedItemPosition();

            if(etSearchValue.length() < 2 && pos < 2) {
                smartToast = new SmartToast(ctx, Toast.makeText(ctx,
                        ctx.getString(R.string.search_other_error), Toast.LENGTH_LONG));
                smartToast.show();
                return;
            } else if (etSearchValue.length() < 10 && pos == 2) {
                smartToast = new SmartToast(ctx, Toast.makeText(ctx,
                        ctx.getString(R.string.search_part_text_error), Toast.LENGTH_LONG));
                smartToast.show();
                return;
            }

            Intent intent = new Intent(ctx, ListSongs.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("type", "search");
            intent.putExtra("typeSearch", MainMenu.this.typeSearch[MainMenu.this.spinner.getSelectedItemPosition()]);
            intent.putExtra("pattern", etSearchValue);
            ctx.startActivity(intent);

        }
    };

    private void addListInit() {
        this.etCommon = (EditText) view.findViewById(R.id.et_add_list);
        this.bCommon = (Button) view.findViewById(R.id.b_add_list);
        this.bCommon.setOnClickListener(addListOCL);
    }

    private View.OnClickListener addListOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String etSearchValue = MainMenu.this.etCommon.getText().toString();
            if(etSearchValue.length() < 1) return;
            DatabaseSystem db = new DatabaseSystem(MainMenu.this.ctx);
            db.openOrCreateDb();
            db.addTableForSongs(etSearchValue);
            db.closeDB();
        }
    };

    private void myListInit() {
        ListView list = new ListView(ctx);
        DatabaseSystem db = new DatabaseSystem(ctx);
        this.linearView.removeAllViews();
        db.openOrCreateDb();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                ctx,
                android.R.layout.simple_list_item_1,
                db.getStringTables()
        );

        list.setAdapter(adapter);
        list.setOnItemClickListener(this.listOICL);
        list.setOnItemLongClickListener(this.listOILCL);
        this.linearView.addView(list);
        db.closeDB();
    }

    private AdapterView.OnItemClickListener listOICL = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Intent intent = new Intent(ctx, ListSongs.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("type", "myList");
            intent.putExtra("listName", parent.getItemAtPosition(position).toString());
            ctx.startActivity(intent);
        }
    };

    private AdapterView.OnItemLongClickListener listOILCL = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            MainMenu.this.selectedItem = parent.getItemAtPosition(position).toString();

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainMenu.this.ctx);
            alertDialogBuilder.setMessage(R.string.dialog_list);
            renameList = new EditText(ctx);
            renameList.setText(MainMenu.this.selectedItem);
            renameList.setId(R.id.et_rename_list);

            alertDialogBuilder.setView(renameList);

            alertDialogBuilder.setPositiveButton(
                    ctx.getResources().getString(R.string.dialog_delete),
                    new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    DatabaseSystem dbms = new DatabaseSystem(MainMenu.this.ctx);
                    dbms.openOrCreateDb();
                    dbms.deleteTable(MainMenu.this.selectedItem);
                    dbms.closeDB();
                    myListInit();
                }
            });

            alertDialogBuilder.setNegativeButton(
                    ctx.getResources().getString(R.string.dialog_rename)
                    , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DatabaseSystem dbms = new DatabaseSystem(MainMenu.this.ctx);
                            dbms.openOrCreateDb();
                            dbms.renameTable(MainMenu.this.selectedItem, renameList.getText().toString());
                            dbms.closeDB();
                            myListInit();
                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return true;
        }
    };

    private void initAddSong(){
        DatabaseSystem dbms = new DatabaseSystem(ctx);
        dbms.openOrCreateDb();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx,
                android.R.layout.simple_spinner_dropdown_item,
                dbms.getStringTables()
                );
        spinner = (Spinner) view.findViewById(R.id.s_add_song);
        spinner.setAdapter(adapter);
        this.bCommon = (Button) view.findViewById(R.id.b_add_song);
        this.bCommon.setOnClickListener(addSongOCL);
    }

    private View.OnClickListener addSongOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            addNewSong();
        }
    };

    private void addNewSong() {

        DatabaseSystem dbms = new DatabaseSystem(ctx);
        String singer, title, text, table;
        spinner = (Spinner) view.findViewById(R.id.s_add_song);
        singer = ((EditText) view.findViewById(R.id.et_add_song_artist)).getText().toString().toUpperCase().trim();
        title = ((EditText) view.findViewById(R.id.et_add_song_title)).getText().toString().toUpperCase().trim();
        text = ((EditText) view.findViewById(R.id.et_add_song_text)).getText().toString().toUpperCase().trim();
        if(singer.length() > minATLength && title.length() > minATLength && text.length()> minTexLength) {
            dbms.openOrCreateDb();
            table = spinner.getSelectedItem().toString();
            dbms.insertSong(table, makeID(singer, title, text), singer, title, text);
        } else {

            Toast.makeText(ctx, ctx.getResources().getString(R.string.add_song_input_error), Toast.LENGTH_SHORT).show();
        }
        dbms.closeDB();
    }

    private String makeID(String singer, String title, String text) {
        int maxText = 4, maxTitle = 24;
        String id = "", newTitle;
        if (maxTitle < title.length()) {
            newTitle = title.substring(0, maxTitle);
        } else {
            newTitle = title;
        }
        id += singer + "-" + newTitle + "-" + text.substring(0, maxText);
        id.replaceAll(" ", "_");
        return id.toUpperCase();
    }

}
