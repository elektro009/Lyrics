package com.ls90dev.lyrics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DatabaseSystem {
    private Context ctx;
    private SQLiteDatabase currentOpenDB;
    private String mainTable;

    public ArrayList<JSONObject> tables;

    public DatabaseSystem( Context ctx) {
        this.ctx = ctx;
        this.mainTable = ctx.getResources().getString(R.string.db_table_main);
    }

    public void openOrCreateDb(){
        this.currentOpenDB = ctx.openOrCreateDatabase(
                ctx.getResources().getString(R.string.db_name),
                ctx.MODE_PRIVATE,
                null);
        this.addMainTable();
        this.readAllTableName();
    }

    public void closeDB() {
        if(this.currentOpenDB != null && currentOpenDB.isOpen()) {
            this.currentOpenDB.close();
        }
    }

    private void readAllTableName() {
        this.tables = new ArrayList<JSONObject>();

        Cursor c = this.currentOpenDB.rawQuery(
                "SELECT id, name FROM " + this.mainTable,
                null);
        if(c.moveToFirst()) {
            while (!c.isAfterLast()) {
                JSONObject value;
                String data = "{\"id\": \"" + c.getString(c.getColumnIndex("id")) + "\"" +
                        ", \"name\":\"" + c.getString(c.getColumnIndex("name")) + "\"" +
                        "}";
                try {
                    value = new JSONObject(data);
                    this.tables.add(value);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                c.moveToNext();
            }
        }
    }

    public void addMainTable() {
        String sql = "create table if not exists " + this.mainTable + "(\n" +
                "id integer primary key autoincrement,\n" +
                "name varchar(128) not null\n" +
                ")";

        this.currentOpenDB.execSQL(sql);
    }

    public void renameTable (String oldName, String newName) {
        String sql = "update " + mainTable + " set name=\""
                + newName + "\" where name=\"" + oldName + "\"";
        this.currentOpenDB.execSQL(sql);
        this.readAllTableName();
    }

    public void addTableForSongs(String tableName){
        String realTableName = this.insertIntoMain(tableName);
        String sql = "create table if not exists " +
                realTableName
                + "(\n" +
                "id varchar(52) not null,\n" +
                "name varchar(128) not null,\n" +
                "title varchar (256) not null,\n" +
                "songText varchar (10240) not null,\n" +
                "creationDate timestamp default current_timestamp,\n" +
                "\n" +
                "primary key(id)\n" +
                ")";

        this.currentOpenDB.execSQL(sql);
        this.readAllTableName();
    }

    public void deleteTable(String table) {
        Toast.makeText(ctx, table + " " + ctx.getResources().getString(R.string.db_delete_table),
                Toast.LENGTH_SHORT).show();
        table = this.getMyListID(table);
        this.deleteRow(this.mainTable, table);

        this.currentOpenDB.execSQL("DROP TABLE IF EXISTS " + "_" + table);
        this.readAllTableName();

    }

    public void deleteRow(String table, String id) {
        if (table.compareTo(mainTable) == 0) {
            table = mainTable;
        } else {
            ArrayList<JSONObject> json = this.getSongs(table, id);
            try {
                Toast.makeText(ctx, json.get(0).getString("name")+ " - " +
                        json.get(0).getString("title") + " " +
                        ctx.getResources().getString(R.string.db_delete_row) + " " +
                        table
                        , Toast.LENGTH_SHORT).show();
                table = "_" + this.getMyListID(table);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.currentOpenDB.delete(
                table, "id=\"" + id + "\"", null);
    }

    public String getMyListID(String tableName) {
        String sql = "SELECT id FROM " + this.mainTable + " WHERE name=\"" + tableName + "\"";

        Cursor c = this.currentOpenDB.rawQuery(sql, null);

        if (c.moveToFirst()) {

            return c.getString(c.getColumnIndex("id"));
        } else return null;
    }

    private String insertIntoMain (String tableName) {
        ContentValues values = new ContentValues();
        String table = this.getMyListID(tableName);
        String addedMessage = ctx.getResources().getString(R.string.db_added_table);
        String existMessage = ctx.getResources().getString(R.string.db_exist_table);

        if (table == null) {
            values.put("name", tableName);
            this.currentOpenDB.insert(this.mainTable, null, values);
            Toast.makeText(ctx, tableName + " " + addedMessage, Toast.LENGTH_SHORT).show();
            return "_" + this.getMyListID(tableName);
        } else {
            //Toast.makeText(ctx, tableName + " " + existMessage, Toast.LENGTH_SHORT).show();
            SmartToast smartToast = new SmartToast(ctx,
                    Toast.makeText(ctx, tableName + " " + existMessage, Toast.LENGTH_SHORT));
            smartToast.show();
        }

        return "_" + table;
    }

    public void insertSong (String table, String id, String singer, String title, String songText) {
        ContentValues values = new ContentValues();
        String existMessage = ctx.getResources().getString(R.string.db_exist_row);
        String addedMessage = ctx.getResources().getString(R.string.db_added_row);
        values.put("id", id);
        values.put("name", singer);
        values.put("title", title);
        values.put("songText", songText);
        if(this.getSongs(table, id) != null) {

            SmartToast smartToast = new SmartToast(ctx,
                    Toast.makeText(ctx,  singer + " " + title + " " + existMessage + " " + table, Toast.LENGTH_SHORT));
            smartToast.show();
            //Toast.makeText(ctx, singer + " " + title + " " + existMessage + " " + table, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(ctx, singer + " - " + title + " " + addedMessage + " " + table, Toast.LENGTH_SHORT).show();
            table = "_" + getMyListID(table);
            this.currentOpenDB.insert(table, null, values);
        }
    }

    public ArrayList<JSONObject> getSongs(String table, String id) {

        ArrayList<JSONObject> values = null;
        table = "_" + this.getMyListID(table);
        String sql = "SELECT id, name, title " + (id == null ? "" : ", songText")  + " FROM " + table;
        if (id != null) {
            sql += " WHERE id=\"" + id + "\"";
        }
        sql += " ORDER BY name";
        Cursor c = this.currentOpenDB.rawQuery(sql, null);

        if (c.moveToFirst()) {
            values = new ArrayList<JSONObject>();
            do {
                String data = "{\"id\":\"" + c.getString(c.getColumnIndex("id")) + "\"" +
                        ", \"name\":\"" + c.getString(c.getColumnIndex("name")) + "\"" +
                        ", \"title\":\"" + c.getString(c.getColumnIndex("title")) + "\"" ;

                if (id != null) {
                    data += ", \"songText\":\"" + c.getString(c.getColumnIndex("songText")) + "\"" ;
                }

                data += "}";
                try {
                    values.add(new JSONObject(data));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } while(c.moveToNext());
        }

        return values;
    }

    public String[] getStringTables() {
        String[] values = new String[this.tables.size()];
        for (int i = 0; i < this.tables.size(); i++) {
            try {
                values[i] = String.valueOf(this.tables.get(i).get("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return values;
    }
}