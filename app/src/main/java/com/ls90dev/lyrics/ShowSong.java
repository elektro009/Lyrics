package com.ls90dev.lyrics;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class ShowSong extends AppCompatActivity {

    private String name = null;
    private String titleSong = null;
    private String songText = null;

    ImageButton bYoutube = null;
    Button bCopy = null;
    TextView tv = null;

    ScaleGestureDetector scaleGestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_song);
        init();
    }

    private void initContent() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        tv = (TextView) findViewById(R.id.tvSongText);
        tv.setMovementMethod(new ScrollingMovementMethod());
        tv.setText(songText);

        bYoutube = (ImageButton) findViewById(R.id.b_youtube);
        bYoutube.setOnClickListener(youtubeOCL);

        bCopy = (Button) findViewById(R.id.bCopySong);
        bCopy.setOnClickListener(bCopyOCL);

        scaleGestureDetector =
                new ScaleGestureDetector(this,
                        new MyOnScaleGestureListener());

        tv.setOnTouchListener(tvOTL);
    }

    private void init() {
        Bundle bundle = getIntent().getExtras();
        name = bundle.getString("name");
        titleSong = bundle.getString("title");
        songText = bundle.getString("songText");
        setTitle(name + " - " + titleSong);
        initContent();
    }

    private View.OnClickListener youtubeOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                Intent intent = new Intent(Intent.ACTION_SEARCH);
                intent.setPackage("com.google.android.youtube");
                intent.putExtra("query", name + " - " + titleSong);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            } catch (Exception e) {
                String message = getResources().getString(R.string.youtube_player);
                Toast.makeText(ShowSong.this, message, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener bCopyOCL = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ClipboardManager myClipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData myClip;
            String text = name + " - " + titleSong + "\n\n\n" + songText;
            SmartToast smartToast = null;

            myClip = ClipData.newPlainText("text", text);
            myClipboard.setPrimaryClip(myClip);
            text = getResources().getString(R.string.copySong);
            smartToast = new SmartToast(ShowSong.this, Toast.makeText(ShowSong.this, text, Toast.LENGTH_SHORT));
            smartToast.show();
        }
    };

    private float deltaSize(float px, boolean typeDelta) {
        float defaultSize = getResources().getDimension(R.dimen.song_default_size);
        float delta = 0.5f;
        int minSize = 15, maxSize = 33;

        px += typeDelta ? delta : -delta;

        if (px <= minSize) {
            return px + delta;
        } else if (px >= defaultSize + maxSize) {
            return px - delta;
        }
        return px;
    }

    private View.OnTouchListener tvOTL = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getPointerCount() > 1) {
                scaleGestureDetector.onTouchEvent(event);
                return true;
            }
            return false;
        }
    };

    public class MyOnScaleGestureListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {

            float scaleFactor = detector.getScaleFactor();
            float size;

            if (scaleFactor > 1) {
                size = deltaSize(tv.getTextSize(), true);
            } else {
                size = deltaSize(tv.getTextSize(), false);
            }
            tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {

        }
    }
}
