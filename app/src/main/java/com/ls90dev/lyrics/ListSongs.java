package com.ls90dev.lyrics;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ListSongs extends AppCompatActivity {

    DatabaseSystem dbms;
    private JSONArray jsonSongs;
    private String[] songs;
    private JSONObject selectedItem = null;
    private int indexTable = -1;
    private String saveInLocal = null;
    private String type = null;
    private String typeSearch = null;
    private String pattern = null;
    private String localTable = null;
    private DownloadWebpageTask downloadWebpageTask = null;
    private static String[] links = null;
    private static int countLinks = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_songs);
        dbms = new DatabaseSystem(this);
        init();
    }

    public void onBackPressed() {
        if (downloadWebpageTask != null) {
            downloadWebpageTask.cancel(true);
        }
        finish();
    }

    // TODO: When you need to publish app in values/string.xml set real ad for banner
    private void initAd() {

        AdView mAdView = (AdView) findViewById(R.id.adView2);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void initLinks() {
        if (links == null) {
            links = getResources().getStringArray(R.array.server_links);
            countLinks = (int)(Math.random()* links.length);
        }
    }

    private void init() {

        Bundle bundle = getIntent().getExtras();
        initAd();
        initLinks();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        type = bundle.getString("type");
        dbms.openOrCreateDb();
        if (type.compareTo("search") == 0) {

            String url = links[countLinks%links.length];
            typeSearch = bundle.getString("typeSearch");
            pattern = updateSpace(bundle.getString("pattern")).toUpperCase();
            url += "?type=rows&" + "typeSearch=" + typeSearch + "&what=" + pattern;
            myClickHandler(url);

        } else {
            localTable =bundle.getString("listName");
            ArrayList<JSONObject> json = dbms.getSongs(localTable, null);
            this.jsonSongs = convertALtoJA(json);
            listSongs();
        }
        dbms.closeDB();
        setTitle(localTable == null ? bundle.getString("pattern") : localTable);
    }

    private void progress (boolean status) {
        LinearLayout ll = (LinearLayout) findViewById(R.id.listSongs);
        ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar);
        int on = View.VISIBLE;
        int off = View.INVISIBLE;
        ll.setVisibility(status ? off : on);
        pb.setVisibility(status ? on : off);
    }

    private void myClickHandler(String url) {
        // Gets the URL from the UI's text field.
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() && url != null) {
            progress(true);
            downloadWebpageTask = new DownloadWebpageTask();
            downloadWebpageTask.execute(url);
            countLinks++;
        } else {
            String messageConnection = getResources().getString(R.string.not_connected);
            Toast.makeText(this, messageConnection, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    // Uses AsyncTask to create a task away from the main UI thread. This task takes a
    // URL string and uses it to create an HttpUrlConnection. Once the connection
    // has been established, the AsyncTask downloads the contents of the webpage as
    // an InputStream. Finally, the InputStream is converted into a string, which is
    // displayed in the UI by the AsyncTask's onPostExecute method.
    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return getResources().getString(R.string.unable_retrieve);
            }
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            //textView.setText(result);
            result = removeGarbage(result);
            JSONArray jsonResult = null;
            try {
                jsonResult = new JSONArray(result);

                if (jsonResult.length() <= 0) {
                    String emptyResultMessage = getResources().getString(R.string.result_empty);
                    Toast.makeText(ListSongs.this, emptyResultMessage, Toast.LENGTH_SHORT).show();
                    finish();
                }
                if (saveInLocal == null) {
                    jsonSongs = jsonResult;
                    listSongs();
                } else {
                    JSONObject obj = jsonResult.getJSONObject(0);
                    if (saveInLocal.compareTo("save") == 0) {
                        dbms.openOrCreateDb();
                        String table = dbms.getStringTables()[indexTable];
                        dbms.insertSong(table, obj.getString("id"), obj.getString("name"),
                                obj.getString("title"), obj.getString("songText"));
                        dbms.closeDB();
                    } else {
                        showSong(obj);
                    }
                    saveInLocal = null;
                }
            } catch (JSONException e) {
                String messageTryAgain = getResources().getString(R.string.try_again);
                Toast.makeText(ListSongs.this, messageTryAgain, Toast.LENGTH_SHORT).show();
                e.printStackTrace();
                //finish();
            }
            progress(false);
        }
        private String downloadUrl(String myurl) throws IOException {
            InputStream is = null;

            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);

                conn.connect();
                int response = conn.getResponseCode();
                //Log.e("TEST", "The response is: " + response);
                is = conn.getInputStream();

                String contentAsString = getStringFromInputStream(is);
                return contentAsString;
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        private String getStringFromInputStream(InputStream is) {
            BufferedReader br = null;
            StringBuilder sb = new StringBuilder();
            String line;

            try {
                br = new BufferedReader(new InputStreamReader(is));
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return sb.toString();
        }

        private String removeGarbage (String rawData) {
            String formattedData = "";
            int index = rawData.indexOf('<');
            if(index > 0) {
                formattedData = rawData.substring(0, index);
                return formattedData;
            } else {
                return rawData;
            }
        }
    }

    private void listSongs () {
        LinearLayout linearView = (LinearLayout) findViewById(R.id.listSongs);
        ListView list = new ListView(this);
        linearView.removeAllViews();
        if (this.jsonSongs == null) return;
        songs = convertJSONToString(this.jsonSongs);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                songs
        );

        list.setAdapter(adapter);
        list.setOnItemClickListener(showOICL);
        if (type.compareTo("search") == 0) {
            list.setOnItemLongClickListener(addInListOILCL);
        } else {
            list.setOnItemLongClickListener(deleteSongOILCL);
        }
        linearView.addView(list);
    }

    private String[] convertJSONToString (JSONArray json) {
        String[] newString = new String[json.length()];
        for (int i = 0; i < json.length(); i++) {
            try {
                JSONObject obj = json.getJSONObject(i);
                newString[i] = obj.getString("name") + " - " + obj.getString("title");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return newString;
    }

    private JSONArray convertALtoJA (ArrayList<JSONObject> jObj) {
        if (jObj == null) return null;
        JSONArray jArr = new JSONArray();
        for (int i = 0; i < jObj.size(); i++) {
            try {
                jArr.put(i, jObj.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jArr;
    }

    AdapterView.OnItemClickListener showOICL = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            try {
                ListSongs.this.selectedItem = jsonSongs.getJSONObject(position);
                String songID = ListSongs.this.selectedItem.getString("id");
                if (type.compareTo("search") == 0) {
                    saveInLocal = "show";
                    String url = links[countLinks%links.length];
                    url += "?type=row&what=" + songID;
                    myClickHandler(url);
                } else {
                    dbms.openOrCreateDb();
                    ArrayList<JSONObject> jsonArrayList = dbms.getSongs(localTable, songID);
                    JSONObject obj = convertALtoJA(jsonArrayList).getJSONObject(0);
                    dbms.closeDB();
                    showSong(obj);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    AdapterView.OnItemLongClickListener deleteSongOILCL = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ListSongs.this);
            try {
                ListSongs.this.selectedItem = jsonSongs.getJSONObject(position);

                alertDialogBuilder.setMessage(
                        getResources().getString(R.string.dialog_delete_song)
                                + ListSongs.this.selectedItem.getString("name") + " - "
                                + ListSongs.this.selectedItem.getString("title"));

                alertDialogBuilder.setPositiveButton(
                        getResources().getString(R.string.dialog_yes),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dbms.openOrCreateDb();
                                try {
                                    dbms.deleteRow(localTable, ListSongs.this.selectedItem.getString("id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                dbms.closeDB();
                                init();
                            }
                        });

                alertDialogBuilder.setNegativeButton(
                        getResources().getString(R.string.dialog_no)
                        , null);

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }
    };

    AdapterView.OnItemLongClickListener addInListOILCL = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ListSongs.this);
            dbms.openOrCreateDb();
            String message = null;
            try {
                ListSongs.this.selectedItem = jsonSongs.getJSONObject(position);
                message = getResources().getString(R.string.dialog_list_decision) +
                        ListSongs.this.selectedItem.getString("name") +
                        " - " + ListSongs.this.selectedItem.getString("title");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            alertDialogBuilder.setTitle(message).
                    setItems(dbms.getStringTables(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String url = links[countLinks%links.length];
                            try {
                                indexTable = which;
                                url += "?type=row&what=" + jsonSongs.getJSONObject(position).getString("id");
                                saveInLocal = "save";
                                dbms.closeDB();
                                myClickHandler(url);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return true;
        }
    };

    private void showSong(JSONObject obj) {
        Intent intent = new Intent(this, ShowSong.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        try {
            progress(false);
            intent.putExtra("id", obj.getString("id"));
            intent.putExtra("name", obj.getString("name"));
            intent.putExtra("title", obj.getString("title"));
            intent.putExtra("songText", obj.getString("songText"));
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // remove space with function removeSpace and change space with %20 for url
    private String updateSpace (String s) {
        String newString = "";
        int lastSpaceIndex = 0, i;
        s = removeSpace(s);
        for (i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                newString += s.substring(lastSpaceIndex, i);
                newString += "%20";
                lastSpaceIndex = i + 1;
            }
        }
        newString += s.substring(lastSpaceIndex, i);
        return newString;
    }

    // remove from start and end position, remove multiple space
    private String removeSpace (String s) {
        int i = 0;
        while(s.charAt(i) == ' ') {
            s = s.substring(i + 1);
        }
        i = s.length() - 1;
        while(s.charAt(i) == ' ') {
            s = s.substring(0, i);
            i--;
        }
        i = 1;
        while (i < s.length()){
            if(s.charAt(i - 1) == ' ' && s.charAt(i) == ' ') {
                s = s.substring(i-1);
            }
            i++;
        }
        return s;
    }
}
